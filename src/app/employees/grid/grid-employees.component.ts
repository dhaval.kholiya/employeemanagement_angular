import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 10, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 10, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 20, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 30, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 40, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 50, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 55, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 60, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 55, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 100, symbol: 'Ne' },
];

@Component({
  selector: 'app-grid-employees',
  templateUrl: './grid-employees.component.html',
  styleUrls: ['./grid-employees.component.css'],
})
export class GridEmployeesComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
  constructor() {}

  ngOnInit(): void {}
}
