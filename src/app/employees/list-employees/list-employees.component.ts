import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css'],
})
export class ListEmployeesComponent implements OnInit {
  currentIndex = -1;
  title = '';

  page = 1;
  count = 0;
  pageSize = 1;
  pageSizes = [1, 2, 3, 6, 9];

  retrieveTutorials(): void {
    // const params = this.getRequestParams(this.title, this.page, this.pageSize);

    // this.tutorialService.getAll(params)
    // .subscribe(
    //   response => {
    //     const { tutorials, totalItems } = response;
    //     this.tutorials = tutorials;
    //     this.count = totalItems;
    //     console.log(response);
    //   },
    //   error => {
    //     console.log(error);
    //   });
    console.log('call');
  }

  handlePageChange(event: number): void {
    this.page = event;
    this.retrieveTutorials();
  }

  handlePageSizeChange(event: any): void {
    this.pageSize = event.target.value;
    this.page = 1;
    this.retrieveTutorials();
  }

  setActiveTutorial(emp: Employee, index: any): void {
    console.log(emp);
    console.log(index);
    this.currentIndex = index;
  }

  employees: Employee[] = [
    {
      Id: 1,
      Name: 'Dhaval',
      Email: 'dk@gmail.com',
      Gender: 'Male',
      Phone: '123456789',
      DateOfBirth: new Date('05/13/1995'),
      IsActive: true,
      PhotoPath: 'assets/images/mark.png',
    },
    {
      Id: 2,
      Name: 'Ekta',
      Email: 'Ekta@gmail.com',
      Gender: 'Female',
      Phone: '9876543210',
      DateOfBirth: new Date('04/10/1994'),
      IsActive: true,
      PhotoPath: 'assets/images/mary.png',
    },
    {
      Id: 3,
      Name: 'Riddhi',
      Email: 'Riddhi@gmail.com',
      Gender: 'Female',
      Phone: '012345679',
      DateOfBirth: new Date('11/12/1993'),
      IsActive: true,
      PhotoPath: 'assets/images/mary.png',
    },
    {
      Id: 4,
      Name: 'Riddhima',
      Email: 'Riddhima@yahoo.com',
      Gender: 'Female',
      Phone: '9876543210',
      DateOfBirth: new Date('05/10/1991'),
      IsActive: true,
      PhotoPath: 'assets/images/mary.png',
    },
    {
      Id: 5,
      Name: 'Ramesh',
      Email: 'Ramesh@gmail.com',
      Gender: 'Male',
      Phone: '012345679',
      DateOfBirth: new Date('01/05/1990'),
      IsActive: true,
      PhotoPath: 'assets/images/john.png',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
