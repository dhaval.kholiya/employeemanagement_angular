export class Employee {
  constructor(
    public Name: String,
    public Id?: number,
    public Gender?: String,
    public Email?: String,
    public Phone?: String,
    public DateOfBirth?: Date,
    public IsActive?: Boolean,
    public PhotoPath?: String
  ) {}
}

export class User {
  constructor(
    public username: String,
    public password: String,
    public id?: String,
    public organisationid?: String,
    public email?: String,
    public phone?: String
  ) {}
}
